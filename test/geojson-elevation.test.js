const fs = require('fs')
const tool = require('../build/geojson-elevation')
var results = require('./data.json').map( (row) => {
  return [row.id, row.elevation, row.geojson]
})

test('Data should countain tests', () => {
  expect(results.length).toBeGreaterThan(0)
})

test.each(results)(
  "Elevation of sample date %# should be greater or equal to zero",
  (id, elevation, geojson) => {
    expect(elevation).toBeGreaterThanOrEqual(0)
  },
)

test.each(results)(
  "Geojson of case %# should be an object",
  (id, elevation, geojson) => {
    expect(typeof geojson).toBe('object')
  },
)

test('Moving Average should be ok', () => {
  const data = [1,5,7,6]
  expect(tool.default.sma(data, 3, 2)).toEqual([4.33,6.00])
})

test('Moving Average precision should be ok', () => {
  const data = [1,5,7,6]
  expect(tool.default.sma(data, 3, 3)).toEqual([4.333,6.000])
})

test('Compute elevation gain should be ok', () => {
  const data = [1,2,3,4,5]
  const func = tool.default.computeElevationGain
  expect(data.reduce(func, 0)).toEqual(4)
})

test('Compute elevation gain should be ok', () => {
  const data = [2000,1000,500,200,0]
  const func = tool.default.computeElevationGain
  expect(data.reduce(func, 0)).toEqual(0)
})

test.each(results)(
  "Elevation of trackID %s - upperBound < %f m > lowerBound",
  (id, elevation, geojson) => {
    const ele = tool.default.getElevationGain(geojson, 4)
    const upperBound = elevation * 1.10
    const lowerBound = elevation * 0.10
    expect(ele).toBeGreaterThanOrEqual(lowerBound)
    expect(ele).toBeLessThanOrEqual(upperBound)
  },
)

