const csv = require('csv-parser')
const fs = require('fs')
const results = [];
 
fs.createReadStream('data.csv')
  .pipe(csv(
    {
        headers: ['elevation', 'geojson', 'id'],
        mapValues: ({ header, index, value }) => {
            if(header === 'geojson'){
                return JSON.parse(value)
            }
            if(header === 'elevation'){
                return parseInt(value)
            }
            return value
        }
    })
  )
  .on('data', (data) => results.push(data))
  .on('end', () => {
    const json = JSON.stringify(results);
    fs.writeFile('data.json', json, 'utf8', function(){
        console.log(`Done ! ${results.length} lines`)
    }); // write it back 
  });
