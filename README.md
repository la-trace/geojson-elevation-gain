# Geojson Elevation Gain

[![pipeline status](https://img.shields.io/gitlab/pipeline/la-trace/geojson-elevation-gain.svg)](https://gitlab.com/la-trace/geojson-elevation/commits/master)
[![version](https://img.shields.io/npm/v/geojson-elevation-gain.svg)](https://www.npmjs.com/package/geojson-elevation-gain.svg)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

Calculate the elevation gain of a Linestring or a Feature
[geosjon](http://geojson.org/). Geojson points need to contain elevation like
`[-3.51732,47.760888,19.8]`. A [Simple Moving
Average](https://en.wikipedia.org/wiki/Moving_average#Simple_moving_average) is
applied on the data before the calculation.

Computing elevation gain is a non trivial thing. The aim of the library is to
find an acceptable compromise for most usecases.

# Install

    npm install --save geojson-elevation-gain

or 

    yarn add geojson-elevation-gain

# Usage

To calculate the elevation gain in meter of geojson:

    getElevationGain(geojson, nbOfPoints)

+ `geojson` is the geojson you want to process, could be a Linestring or a
  feature with a LineString as geometry.
+ `nbOfPoints` is the width of the SMA.

# Development

    yarn install

# Roadmap

- [x] Make it works
- [ ] Make it right
- [ ] Better data handling (Bad/No elevation detection)
- [ ] Multiple algorithms

# Tests

Tests are performed across a bunch of reference files coming from la-trace.com.
Difference between the result of the algorithm and the reference value should
be lower than 10% to be considered as correct.

    yarn run build
    yarn run test