# [0.6.0](https://gitlab.com/la-trace/geojson-elevation-gain/compare/v0.5.3...v0.6.0) (2018-12-26)


### Features

* add junit to tests ([507da21](https://gitlab.com/la-trace/geojson-elevation-gain/commit/507da21))
* add junit to tests ([fc309b5](https://gitlab.com/la-trace/geojson-elevation-gain/commit/fc309b5))

## [0.5.3](https://gitlab.com/la-trace/geojson-elevation-gain/compare/v0.5.2...v0.5.3) (2018-12-24)


### Bug Fixes

* changelog generation, again ([a5742ae](https://gitlab.com/la-trace/geojson-elevation-gain/commit/a5742ae))
